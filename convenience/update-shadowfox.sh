#!/usr/bin/env bash
#Requires unzip, jq, curl, and xargs
#Install pv to view progress of downloads
rm ./shadow* && echo "[update.sh] Removed old version of shadowfox updater tool."
stack_url="https://api.github.com/repos/SrKomodo/shadowfox-updater/releases/latest"
xargs curl -O -J -L < <(jq --raw-output '.assets[1] | .browser_download_url' < <(curl -s $stack_url))
mv shadowfox_linux_x64 shadowfox && chmod u+x shadowfox && ./shadowfox
