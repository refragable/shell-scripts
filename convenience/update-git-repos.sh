#!/bin/bash
#updates multiple git repos easily
#place in root directory and run this script
#/home/user/git/learnbyexample/ <-- RUN HERE TO UPDATE ALL BELOW
#├── Command-line-text-processing
#├── curated_resources
#├── Linux_command_line
#├── Perl_intro
#├── Python_Basics
#├── scripting_course
#└── vim_reference

while read gitrepo; do
 cd $gitrepo && git pull && cd ..
done < <(find . -maxdepth 1 -mindepth 1 -type d -print)