#!/usr/bin/python3 -B

import json
import lzstring
import pprint
import sys, getopt

x = lzstring.LZString()

if len(sys.argv) == 2:
    s = sys.argv
#    print(s[1]) # prints the second item in the array
    print(x.compressToBase64(s[1]))
elif len(sys.argv) > 2:
    print("Warning: This script takes only one argument.")
    print("Assuming the first given argument is the string to decompress.")
    s = sys.argv
    print(x.compressToBase64(s[1]))
else:
    s = input('Enter string to decode: ')
    print(x.compressToBase64(s))
