#!/bin/bash
#delete lots of folders, fast. now 100% safer

read -p "Permanently delete subdirs? " -n 1 -r # n flag accepts ONE letter and automatically runs
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
 xargs -n 1 -P 4 -0 -I{} rm -rf {} < <(find ./ -maxdepth 1 -mindepth 1 -type d -print0) && echo "Deleted subdirs!" || echo "Something went wrong with deletion!"
    exit 1
fi
