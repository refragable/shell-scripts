#!/bin/bash
# Removes old revisions of snaps
# CLOSE ALL SNAPS BEFORE RUNNING THIS
# script originally by Popey https://superuser.com/a/1330590
set -eu

LANG=C snap list --all | awk '/disabled/{print $1, $3}' |
    while read snapname revision; do
        snap remove "$snapname" --revision="$revision"
    done

# uncomment the following to retain TWO old
# versions instead of three
# snap set system refresh.retain=2

# also remove unused flatpaks
flatpak uninstall --unused
