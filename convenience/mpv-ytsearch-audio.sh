#!/bin/bash

if [[ ! "$@" ]]
then
  echo "Please enter a search term to begin downloading:"
  read ytSearch
  echo "Please enter the number of results to play:"
  read ytResultsNum
  mpv --no-video --force-window=no --no-audio-display --pause=no ytdl://ytsearch"$ytResultsNum":"$ytSearch"
else
  args="$*"
  mpv --no-video --force-window=no --no-audio-display --pause=no ytdl://ytsearch:"$args"
fi
