#!/usr/bin/env bash
#Requires unzip, jq, curl, and xargs
#Install pv to view progress of downloads
rm ./rattle* && echo "[update.sh] Removed old version of stack tool."
stack_url="https://api.github.com/repos/dan-v/rattlesnakeos-stack/releases/latest"
xargs curl -O -J -L < <(jq --raw-output '.assets[0] | .browser_download_url' < <(curl -s $stack_url))
unzip *.zip  && ./rattlesnakeos-stack deploy
