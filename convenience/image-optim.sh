#!/bin/bash
# This script will optimize all jpg/png files
# Requires jpegoptim and optipng

while read image; do
 jpegoptim -s "$image"
done < <(find . -maxdepth 2 -type f \( -name "*.jpg" -or -name "*.jpeg" \))

while read image; do
 tmpfile=$(mktemp)
 zopflipng --iterations=50 --keepchunks=iCCP --lossy_transparent --splitting=3 -my $image $tmpfile
 mv $tmpfile $image
done < <(find . -maxdepth 2 -type f \( -name "*.png" \))
