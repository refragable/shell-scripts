#!/usr/bin/env bash
#Requires unzip, jq, curl, and xargs
#Install pv to view progress of downloads
mkdir -p ./old
mv ./*.deb ./old/ && echo 'Downloading the latest VSCodium release.'
release_api_url="https://api.github.com/repos/VSCodium/vscodium/releases/latest"
xargs curl -O -J -L < <(jq --raw-output '.assets[8] | .browser_download_url' < <(curl -s $release_api_url))
sudo apt -f install ./vscodium*.deb
