#!/usr/bin/env bash
# create lots of softlinks recursively from many to one
# requires gnu copy for soft symlinks

## config ver.
# specify the origin and destination dirs
orig_dir=
dest_dir=

while read lines; do
cp -rs "$lines" "$dest_dir" # TEST FIRST
# echo "$lines" would be symlinked into "$dest_dir" # testing line
# change the maxdepth/mindepth and type
# currently configured for base directory two levels above
done < <(find "$orig_dir" -maxdepth 2 -mindepth 2 -type d)
