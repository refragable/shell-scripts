#!/bin/bash
# bash wrapper to invoke python3 fork of mcomix
# all dependencies are managed through a virtualenv

source ~/.virtualenvs/mcomix/bin/activate
python ~/src/mcomix3/mcomix/mcomixstarter.py "$1"
