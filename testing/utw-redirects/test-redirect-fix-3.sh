#!/bin/bash
#temporarily create redirects for old, non-slug links
#this portion can be removed after gitlab fixes custom 404s
echo [build.sh] Generating redirects…
<src/redirect/redirect.txt xargs -I % cp src/redirect/redirect.html %
#fix redirects to slugs instead of wiki index
while read r; do 
	sed -i "s|gitlab.io\/|gitlab.io\/${r%.html}\/|g" $r 
	sed -i "s|gitlab.io\/public\/|gitlab.io\/|g" $r
done <src/redirect/redirect.txt

#note: the above is what made it into the main UTW repo