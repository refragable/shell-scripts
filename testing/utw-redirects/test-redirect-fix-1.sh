#!/bin/bash

#fix redirect generation
cat -n src/redirect/redirect.txt | while read n f; do find public/ -name $f && sed -i "s|gitlab.io\/|gitlab.io\/${f%.html}\/|g" $f; done

cat -n src/redirect/redirect.txt | while read n f; do find public/ -name $f && sed -i "s|gitlab.io\/|gitlab.io\/${f%.html}\/|g" $f; done

#note: This is the first iteration of my 2nd redirect fix.

#temporarily create redirects for old, non-slug links
#this portion can be removed after gitlab fixes custom 404s
#echo [build.sh] Generating redirects…
#cat -n src/redirect/redirect.txt | while read n f; do cp src/redirect/redirect.html $f; done