#!/bin/bash
#temporarily create redirects for old, non-slug links
#this portion can be removed after gitlab fixes custom 404s
echo [build.sh] Generating redirects…
<src/redirect/redirect.txt xargs -I % cp src/redirect/redirect.html %
#note: the previous script used CAT, which fails to return the last argument if the source file does not end with a newline.

#fix redirects to slugs instead of wiki index
cat -n src/redirect/redirect.txt | while read n f; do find public/ -name $f && sed -i "s|gitlab.io\/|gitlab.io\/${f%.html}\/|g" $f && sed -i "s|gitlab.io\/public\/|gitlab.io\/|g" $f; done
#note: I was bothered by the UUOC, therefore fix-3 was made.

#REDIRECT SANITY CHECK
<src/redirect/redirect.txt xargs -I % grep "url\=https\:\/\/.*\/" %
#note: the funny thing about the slug fix is that
#it caused the links in the footer to get edited
#so instead of fixing the sed command, I made the
#grep regex more specific (lazy)