#!/bin/bash
#create redirects since custom error pages are broken
#echo [build.sh] Running pandoc for redirects…
#FILES=$(find src/md -type f)
#for FILE in $FILES
#do
##  pandoc --template=src/res/redirect.html $FILE -o public/$(basename ${FILE##*/} .md).html
#    pandoc --template=src/res/redirect.html $FILE -o redirect/$(echo $FILE | sed -r "s/src\/md\/(.+)\.md/\1/").html
#done


#note: I couldn't get my webserver to honor symlinks
#therefore this solution was abandoned
echo [build.sh] Running ln for redirects…
FILES=$(find src/md -type f)
for FILE in $FILES
do
  ln -s /301.html public/$(echo $FILE | sed -r "s/src\/md\/(.+)\.md/\1/").html
done