#!/bin/bash
#temporarily create redirects for old, non-slug links
#this portion can be removed after gitlab fixes custom 404s
echo [build.sh] Generating redirects…
while read n f <(cat -n src/redirect/redirect.txt)
  do 
  	cp src/redirect/redirect.html $f
done

#note: this was my initial merge request
#the last line was dropped due to the quirks of cat