#!/bin/sh -e 
find . -name '*.flac' -print0 | xargs -0 -n1 sh -c '
INFILE="$1"
META=$( metaflac --show-tag=REPLAYGAIN_TRACK_GAIN "$INFILE" )
if [ -z "$META" ] ; then
  echo no rp tags for "$INFILE"
fi' ''
