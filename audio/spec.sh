#!/bin/bash
# spectral generation using sox
mkdir -p ./Spectrograms
specs=0
for f in *.flac
	do sox "$f" -n remix 1 spectrogram -x 3000 -y 513 -z 120 -w Kaiser -o Spectrograms/"$f"-full.png
	# edit the time after the -S flag for a different section of the track.
	sox "$f" -n remix 1 spectrogram -X 500 -y 1025 -z 120 -w Kaiser -S 1:00 -d 0:02 -o Spectrograms/"$f"-zoom.png
	((specs++))
	echo "Spectrals made for "$specs" track(s)."
done