# NOTICE: THESE SCRIPTS HAVE BEEN RELICENSED TO GPL v3 OR LATER

If you want to transcode your entire library, I recommend using [acxi](https://github.com/smxi/acxi) which is a fantastic tool. Additionally, [redoflacs](https://github.com/sirjaren/redoflacs) is great for cleaning up your flac files (compression, replaygain, etc).

The folder contains some simple bash scripts to automate some common audio file tasks. 

## Scripts

### opus.sh
This script takes no arguments. Simply run it within the target directory to transcode all FLAC tracks to VBR 128 opus.

### spec.sh
This script takes no arguments. Simply run it within the target directory to generate two spectrograms per each FLAC track.
