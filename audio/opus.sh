#!/bin/bash
# This script converts all FLAC files in a directory to 128kbps opus

for file in ./*.flac; do
    opusenc --bitrate 128 "$file" "${file%.flac}.opus"
done

dir="$PWD"-opus
mkdir -p "$dir" || exit

cp ./*.png ./*.jpg "$dir"
#for img in .; do
#	if [ ${img: -4} == ".jpg" ]
#	then
#		cp "$img" "$dir"
#	elif [ ${img: -5} == ".jpeg" ]
#	then
#		cp "$img" "$dir"
#	elif [ ${img: -4} == ".png" ]
#	then
#		cp "$img" "$dir"
#	else
#		echo "No images found in directory, please find add album art to " ${dir%"-opus"}
#	fi
#done

mv ./*.opus "$dir"
mv "$dir"/ "$HOME"/Music/transcode-opus/

